package tareatdd.tareatdd.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import tareatdd.tareatdd.utils.Usuario;

@RestController
public class userController {
    @PostMapping("/usuarios")
    public ResponseEntity<String> crearUsuario(@RequestBody Usuario usuario) {
        // Aquí puedes realizar las validaciones y el almacenamiento de los datos del usuario
        // en una base de datos u otro almacenamiento persistente

        // Ejemplo de validación básica de campos requeridos
        if (usuario.getNombre() == null || usuario.getNombre().isEmpty() ||
                usuario.getApellidoPaterno() == null || usuario.getApellidoPaterno().isEmpty() ||
                usuario.getRut() == null || usuario.getRut().isEmpty()) {
            return ResponseEntity.badRequest().body("Nombre, apellido paterno y RUT son campos requeridos");
        }

        // Ejemplo de almacenamiento exitoso
        // userRepository.save(usuario);

        return ResponseEntity.status(HttpStatus.CREATED).body("Usuario creado exitosamente");
    }
}
