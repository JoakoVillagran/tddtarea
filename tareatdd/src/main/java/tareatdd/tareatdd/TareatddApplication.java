package tareatdd.tareatdd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TareatddApplication {

	public static void main(String[] args) {
		SpringApplication.run(TareatddApplication.class, args);
	}

}
